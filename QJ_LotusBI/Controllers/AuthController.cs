﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using QJY.API;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace QJ_LotusBI.Controllers
{
    /// <summary>
    /// 系统基础框架接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IHttpContextAccessor _accessor;
        private IHostingEnvironment environment { get; set; }
        private IContentTypeProvider contentTypeProvider { get; set; }


        Msg_Result Model = new Msg_Result() { Action = "", ErrorMsg = "" };


        public AuthController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;

        }

        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="PostData">上传数据</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> Login(Object PostData)
        {

            JObject JsonData = JObject.FromObject(PostData);
            string username = JsonData["UserName"] == null ? "" : JsonData["UserName"].ToString();
            string password = JsonData["password"] == null ? "" : JsonData["password"].ToString();

            string iswx = JsonData["iswx"] == null ? "" : JsonData["iswx"].ToString(); //1.判断小程序登录
            string openid = JsonData["openid"] == null ? "" : JsonData["openid"].ToString();

            string avatarUrl = JsonData["avatarUrl"] == null ? "" : JsonData["avatarUrl"].ToString();
            string nickName = JsonData["nickName"] == null ? "" : JsonData["nickName"].ToString();
            Dictionary<string, string> results3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(PostData.ToString());
            Model.ErrorMsg = "";
            JH_Auth_QY qyModel = new JH_Auth_QYB().GetALLEntities().First();
            password = CommonHelp.GetMD5(password);
            JH_Auth_User userInfo = new JH_Auth_User();
            List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password).ToList();
            if (userList.Count() == 0)
            {
                Model.ErrorMsg = "用户名或密码不正确";
            }
            else
            {
                userInfo = userList[0];
                if (userInfo.IsUse != "Y")
                {
                    Model.ErrorMsg = "用户被禁用,请联系管理员";
                }
                if (Model.ErrorMsg == "")
                {
                    Model.Result = JwtHelper.CreateJWT(username, "Admin");
                    Model.Result1 = userInfo.UserName;
                    Model.Result2 = qyModel.FileServerUrl;

                    Model.Result4 = userInfo;

                    if (iswx == "1")
                    {
                        //修改openid
                        userInfo.weixinCard = openid;
                        userInfo.txurl = avatarUrl;
                        userInfo.NickName = nickName;
                        new JH_Auth_UserB().Update(userInfo);
                    }
                    CacheHelp.Remove(userInfo.UserName);

                }

            }


            return ControHelp.CovJson(Model); ;
        }




        public ActionResult<string> Login_For_WX(Object PostData)
        {

            JObject JsonData = JObject.FromObject(PostData);
            string openid = JsonData["openid"] == null ? "" : JsonData["openid"].ToString();
            string xmid = JsonData["xmid"] == null ? "" : JsonData["xmid"].ToString();

            Dictionary<string, string> results3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(PostData.ToString());
            Model.ErrorMsg = "";
            JH_Auth_QY qyModel = new JH_Auth_QYB().GetALLEntities().First();

            var userInfo = new JH_Auth_UserB().GetEntities(d => d.weixinCard == openid).FirstOrDefault();
            if (userInfo != null)
            {
                if (userInfo.IsUse != "Y")
                {
                    Model.ErrorMsg = "用户被禁用,请联系管理员";
                }
                if (Model.ErrorMsg == "")
                {
                    Model.Result = JwtHelper.CreateJWT(userInfo.UserName, "Admin");
                    Model.Result2 = userInfo;


                }

            }


            return ControHelp.CovJson(Model); ;
        }
        /// <summary>
        /// 获取授权信息接口,用于和云盘整合
        /// </summary>
        /// <param name="PostData">授权信息</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> AuthUser(Object PostData)
        {

            JObject JsonData = JObject.FromObject(PostData);
            string username = JsonData["UserName"] == null ? "" : JsonData["UserName"].ToString();
            string Secret = JsonData["Secret"] == null ? "" : JsonData["Secret"].ToString();
            string lotusBISecret = CommonHelp.GetConfig("lotusBISecret", "chengyan1215");
            if (Secret == lotusBISecret)
            {
                JH_Auth_QY qyModel = new JH_Auth_QYB().GetALLEntities().First();
                JH_Auth_User userInfo = new JH_Auth_User();
                List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.UserName == username).ToList();
                if (userList.Count() == 0)
                {
                    Model.ErrorMsg = "无效得用户名";
                }
                else
                {
                    userInfo = userList[0];
                    if (userInfo.IsUse != "Y")
                    {
                        Model.ErrorMsg = "用户被禁用,请联系管理员";
                    }
                    if (Model.ErrorMsg == "")
                    {
                        Model.Result = JwtHelper.CreateJWT(username, "Admin");
                        Model.Result1 = userInfo.UserName;
                        Model.Result2 = qyModel.FileServerUrl;
                        Model.Result4 = userInfo;
                        CacheHelp.Remove(userInfo.UserName);
                    }

                }
            }
            else
            {
                Model.ErrorMsg = "Secret信息有误";
            }

            return ControHelp.CovJson(Model); ;
        }



        /// <summary>
        /// 获取访客授权信息接口
        /// </summary>
        /// <param name="PostData">授权信息</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> VisitorUser(Object PostData)
        {

            JObject JsonData = JObject.FromObject(PostData);
            string token = JsonData["token"] == null ? "" : JsonData["token"].ToString();

            var models = new BI_DB_YBPB().GetEntities(d => d.Remark == token).ToList();
            if (models.Count() > 0)
            {
                Model.Result = JwtHelper.CreateJWT("VisitorUser", "Visitor");
                Model.Result1 = models[0].ID;
            }
            else
            {
                Model.ErrorMsg = "Secret信息有误";
            }

            return ControHelp.CovJson(Model); ;
        }


        /// <summary>
        /// 执行系统基础框架接口
        /// </summary>
        /// <param name="Action">操作Action</param>
        /// <param name="PostData">参数Json数据</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult<string> ExeAction(string Action, Object PostData)
        {
            Model.Action = Action;
            var context = _accessor.HttpContext;
            var tokenHeader = context.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            TokenModelJWT tokenModel = JwtHelper.SerializeJWT(tokenHeader);

            if (new DateTimeOffset(DateTime.Now.AddMinutes(5)).ToUnixTimeSeconds() > tokenModel.Exp)
            {
                //需要更新Token
                Model.uptoken = JwtHelper.CreateJWT(tokenModel.UserName, "Admin");
            }
            JH_Auth_UserB.UserInfo UserInfo = CacheHelp.Get(tokenModel.UserName) as JH_Auth_UserB.UserInfo;
            if (UserInfo == null)
            {
                UserInfo = new JH_Auth_UserB().GetUserInfo(10334, tokenModel.UserName);
                CacheHelp.Set(tokenModel.UserName, UserInfo);
            }
            try
            {

                JObject JsonData = JObject.FromObject(PostData);
                string P1 = JsonData["P1"] == null ? "" : JsonData["P1"].ToString();
                string P2 = JsonData["P2"] == null ? "" : JsonData["P2"].ToString();

                //Dictionary<string, string> results3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(PostData.ToString());
                var function = Activator.CreateInstance(typeof(AuthManage)) as AuthManage;
                var method = function.GetType().GetMethod(Action.ToUpper());
                method.Invoke(function, new object[] { JsonData, Model, P1, P2, UserInfo });
                new JH_Auth_LogB().InsertLog("系统接口", context.Request.Path + "/" + Model.Action, ControHelp.CovJson(Model), UserInfo.User.UserName, context.Request.Body.ToString(), UserInfo.QYinfo.ComId, context.Connection.RemoteIpAddress.ToString());
            }
            catch (Exception ex)
            {
                Model.ErrorMsg = "接口调用失败,请检查日志" + ex.StackTrace.ToString();
                Model.Result = ex.ToString();
                new JH_Auth_LogB().InsertLog("错误日志", context.Request.Path + "/" + Model.Action, Model.ErrorMsg, tokenModel.UserName, context.Request.Body.ToString(), 0, context.Connection.RemoteIpAddress.ToString());
            }

            return ControHelp.CovJson(Model);
        }





        /// <summary>
        /// 导入用户模板
        /// </summary>
        /// <param name="excelfile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> IMPORTUSER(IFormFile excelfile)
        {
            try
            {
                var files = Request.Form.Files;
                var file = files.FirstOrDefault();
                using (var memoryStream = new MemoryStream())
                {

                    file.CopyTo(memoryStream);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    string headrow = Request.Form["headrow"].ToString();
                    string suffix = file.FileName.Split('.')[1];
                    Model.Result = new CommonHelp().ExcelToTable(memoryStream, int.Parse(headrow), suffix);
                }

            }
            catch (Exception ex)
            {
                Model.ErrorMsg = "IMPORTUSER导入失败";
                Model.Result = ex.ToString();
                new JH_Auth_LogB().InsertLog("IMPORTUSER", Model.ErrorMsg + ex.StackTrace.ToString(), ex.ToString(), "", "", 0, "");
                return "";
            }


            return ControHelp.CovJson(Model);
        }


     

        //[HttpGet("download-file")]
        //public FileResult downloadFile(string name, string display)
        //{
        //    string contentType;
        //    contentTypeProvider.TryGetContentType(name, out contentType);
        //    HttpContext.Response.ContentType = contentType;
        //    string path = environment.WebRootPath + @"\images\" + name; // 注意哦, 不要像我这样直接使用客户端的值来拼接 path, 危险的 
        //    FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes(path), contentType)
        //    {
        //        FileDownloadName = display
        //    };
        //    // return File("~/excels/report.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "report.xlsx"); // 返回 File + 路径也是可以, 这个路径是从 wwwroot 走起 
        //    // return File(await System.IO.File.ReadAllBytesAsync(path), same...) // 或则我们可以直接返回 byte[], 任意从哪里获取都可以. 

        //    return result;
        //}

        #region 水费管理
        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="PostData">上传数据</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> SFLogin(Object PostData)
        {

            JObject JsonData = JObject.FromObject(PostData);
            string username = JsonData["username"] == null ? "" : JsonData["username"].ToString();
            string password = JsonData["password"] == null ? "" : JsonData["password"].ToString();
            DataTable dtyh = new DataTable();

            dtyh = new SFJSManage().GetUserInfo(username + "," + password);
            if (dtyh.Rows.Count == 0)
            {
                Model.ErrorMsg = "用户名或密码不正确";
            }
            else
            {
                Model.Result = dtyh;

                string strUserType = dtyh.Rows[0]["usertype"].ToString();
                string strxzids = dtyh.Rows[0]["czids"].ToString();

                string strTemp = strUserType;
                if (strUserType== "斗长"|| strUserType == "管理局人员")
                {
                    strTemp = "管理局人员";//斗长当作管理局人员处理
                }

                DataTable dt = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE ggtype='新闻资讯' and jsjs LIKE '%" + strTemp + "%' ORDER BY ID DESC");
                DataTable dt1 = new JH_Auth_QYB().GetDTByCommand("SELECT   id,title,fbdate,imgurl,lls FROM qj_sys_tzgg WHERE  ggtype='新闻资讯'and jsjs LIKE '%" + strTemp + "%' AND islb='是' ORDER BY ID DESC");
                DataTable dt3 = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate FROM qj_sys_tzgg WHERE  ggtype='系统公告' and  jsjs LIKE '%" + strTemp + "%' ORDER BY ID DESC");

                DataTable dtyys = new DataTable(); //应用
                dtyys.Columns.Add("yytype");
                dtyys.Columns.Add("yyname");
                dtyys.Columns.Add("yyico");
                dtyys.Columns.Add("url");
                if (strUserType == "村组管理员")
                {
                    dtyys.Rows.Add(new object[] { "0", "计量计费", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "用户管理", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "上报历史", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "水价说明", "", "" });
                }
                else if (strUserType == "斗长")
                {
                    dtyys.Rows.Add(new object[] { "0", "计量计费", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "用户管理", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "上报历史", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "水价说明", "", "" });
                }
                else
                {
                    dtyys.Rows.Add(new object[] { "0", "用户管理", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "上报历史", "", "" });
                    dtyys.Rows.Add(new object[] { "0", "水价说明", "", "" });
                }
                Model.Result1 = dtyys;
                Model.Result2 = dt1;
                Model.Result3 = dt;


                DataTable dtdb = new JH_Auth_QYB().GetDTByCommand("SELECT distinct did,dbname FROM qj_cz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");

                DataTable dtcz = new JH_Auth_QYB().GetDTByCommand("SELECT id,xzname,pname,did,dbname,(pname+'/'+xzname) AS xz FROM qj_cz  WHERE   id IN ('" + strxzids.ToFormatLike() + "')");
                dtdb.Columns.Add("xzdata", Type.GetType("System.Object"));
                for (int i = 0; i < dtdb.Rows.Count; i++)
                {
                    dtdb.Rows[i]["xzdata"] = dtcz.Where("did='" + dtdb.Rows[i]["did"].ToString() + "'");
                }

                Model.Result4 = dtdb;
                Model.Result6 = dt3;




                JH_Auth_Branch Branch = new JH_Auth_BranchB().GetEntities(d => d.DeptCode == 1).FirstOrDefault();
                if (Branch != null)
                {
                    Model.Result5 = Branch.Remark2;
                }

            }
            return ControHelp.CovJson(Model); ;
        }


        /// <summary>
        /// 获取APP版本
        /// </summary>
        /// <param name="PostData">上传数据</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> GetAPPV(Object PostData)
        {
            Model.Result = "1.1";
            Model.Result1 = "http://bjx.qijiekeji.com/MobWeb/app-release.apk";
            Model.Result2 = "系统更新内容";
            return ControHelp.CovJson(Model); ;
        }
        #endregion





    }
}