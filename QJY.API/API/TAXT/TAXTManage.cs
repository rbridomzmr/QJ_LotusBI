﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace QJY.API
{
    public class TAXTManage
    {

        #region PC端


        //获取短信内容

        public void DRUSERDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strSQL = "SELECT  *  FROM qj_tian  WHERE intProcessStanceid='0' ";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strID = dt.Rows[i]["ID"].ToString();
                string strRealName = dt.Rows[i]["CRUserName"].ToString();
                string strCRUser = new JH_Auth_UserB().GetUserByUserRealName(10334, strRealName);
                string strfyr = "";
                foreach (string item in dt.Rows[i]["tafyrname"].ToString().Split(','))
                {
                    strfyr = strfyr + new JH_Auth_UserB().GetUserByUserRealName(10334, item) + ",";
                }

                new JH_Auth_UserB().ExsSclarSql("UPDATE qj_tian SET CRUser='" + strCRUser + "',tafyr='" + strfyr.Trim(',') + "' WHERE ID='" + strID + "'");
            }

            //DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);

            //for (int i = 0; i < dtalldata.Rows.Count; i++)
            //{

            //    string tar = dtalldata.Rows[i][1].ToString();
            //    string tadw = dtalldata.Rows[i][2].ToString();

            //    string tabh = dtalldata.Rows[i][3].ToString();
            //    string fyr = dtalldata.Rows[i][4].ToString();
            //    fyr = fyr.Replace('\n', ',').Replace(" ", "");
            //    string ay = dtalldata.Rows[i][5].ToString();
            //    string nr = dtalldata.Rows[i][6].ToString();
            //    string cs = dtalldata.Rows[i][7].ToString();
            //    string isla = dtalldata.Rows[i][8].ToString().Contains("是") ? "同意立案" : "不同意立案";

            //    string lwyj = dtalldata.Rows[i][9].ToString();

            //    string talb = dtalldata.Rows[i][10].ToString();
            //    if (talb.Trim() == "")
            //    {
            //        talb = "其他类";
            //    }
            //    else
            //    {
            //        talb = talb.Substring(2);
            //    }
            //    string zbdw = dtalldata.Rows[i][11].ToString();

            //    string xbdw = dtalldata.Rows[i][12].ToString();

            //    string zbyj = dtalldata.Rows[i][13].ToString();
            //    string zbpj = dtalldata.Rows[i][14].ToString();

            //    new Yan_WF_PIB().ExsSclarSql("INSERT INTO  [qj_tian] ( [CRUser], [DCode], [DName], [CRUserName], [CRDate], " +
            //        "[intProcessStanceid], [ComID], [tabh], [tatitle], [tafyrname], [tacontent], [tacs], [isla], [zbdw], [zbr]," +
            //        " [zbyj], [myd], [tarfy], [tawyj], [tatzname], [talb], [xbdw]) VALUES ('chengyan', '4116', '" + tadw + "', '" + tar + "'," +
            //        " '2020-09-20 23:07:14.283', '0', '10334', '" + tabh + "',  '" + ay + "','" + fyr + "', '" + nr + "', '" + cs + "', '" + isla + "', '" + zbdw + "', 'zbr01', " +
            //        "'" + zbyj + "', '" + zbpj + "', '附议', '" + lwyj + "', '第八届教代会提案', '" + talb + "', '" + xbdw + "');");




            //}


            //DataView dataView = dtalldata.DefaultView;

        }
        public void TBUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtJG = new JH_Auth_UserB().GetDTByCommand("SELECT DISTINCT JGMC,JGDM  FROM vw_users");
            for (int i = 0; i < dtJG.Rows.Count; i++)
            {
                int strJGDM = int.Parse(dtJG.Rows[i]["JGDM"].ToString());
                string strJGMC = dtJG.Rows[i]["JGMC"].ToString();

                if (new JH_Auth_BranchB().GetEntities(D => D.DeptCode == strJGDM).Count() == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO [jh_auth_branch] ([ComId], [DeptCode], [DeptName], [DeptShort], [DeptDesc], [DeptRoot], [BranchLevel], [Remark1], [Remark2], [BranchLeader], [WXBMCode], [TXLQX], [RoleQX], [IsHasQX], [RoomCode], [CRUser], [CRDate]) VALUES ('10334', '" + strJGDM + "', N'" + strJGMC + "', '2', N'" + strJGMC + "', '1', '0', N'1-" + strJGDM + "', N'', N'', NULL, N'', N'', N'', N'', N'administrator', '2020-07-26 22:24:01.000');");
                }
            }
            DataTable dtUser = new JH_Auth_UserB().GetDTByCommand("SELECT jsdm,JSXM,jgdm,jsxb  FROM vw_users");

            for (int i = 0; i < dtUser.Rows.Count; i++)
            {
                string jsdm = dtUser.Rows[i]["jsdm"].ToString();
                string strJSXM = dtUser.Rows[i]["JSXM"].ToString();
                int jgdm = int.Parse(dtUser.Rows[i]["jgdm"].ToString());
                string jsxb = dtUser.Rows[i]["jsxb"].ToString();

                JH_Auth_User user = new JH_Auth_UserB().GetEntity(D => D.UserName == jsdm);
                if (user != null)
                {
                    if (user.BranchCode != jgdm)
                    {
                        user.BranchCode = jgdm;
                        new JH_Auth_UserB().Update(user);
                    }

                }
                else
                {
                    user = new JH_Auth_User();
                    user.BranchCode = jgdm;
                    user.UserName = jsdm;
                    user.UserRealName = strJSXM;
                    user.Sex = jsxb;
                    user.UserPass = "E99A18C428CB38D5F260853678922E03";
                    user.IsUse = "Y";
                    user.remark = "1";
                    user.ComId = 10334;
                    user.UserOrder = 0;
                    user.CRUser = "administrator";
                    user.CRDate = DateTime.Now;

                    new JH_Auth_UserB().Insert(user);
                }
            }


        }

        /// <summary>
        /// 获取最新的提案通知
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETTADATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = UserInfo.User.UserName;
            string strSQL = "SELECT TOP 1 *  FROM qj_tatz ORDER BY ID DESC  ";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            msg.Result = dt;
            string isTA = "N";
            if (dt.Rows.Count > 0)
            {
                DateTime dts = DateTime.Parse(dt.Rows[0]["sdate"].ToString());
                DateTime dte = DateTime.Parse(dt.Rows[0]["edate"].ToString());
                if (DateTime.Now > dts && DateTime.Now < dte)
                {
                    isTA = "Y";
                }

            }
            msg.Result1 = isTA;
        }


        /// <summary>
        /// 获取我创建的提案数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETLCDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = UserInfo.User.UserName;
            string strType = P2;
            string strWhere = " AND  Yan_WF_PI.CRUser='" + strUserName + "'";
            string strSQL = " SELECT Yan_WF_PI.*,Yan_WF_PD.ProcessName,Yan_WF_PD.ProcessClass,Yan_WF_PD.ProcessType FROM  Yan_WF_PI  INNER JOIN Yan_WF_PD ON Yan_WF_PI.PDID =Yan_WF_PD.ID  WHERE PDID='257'  " + strWhere + "  ORDER BY Yan_WF_PI.ID DESC";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            dt.Columns.Add("TaskName");
            dt.Columns.Add("TaskAssInfo");
            dt.Columns.Add("Taskorder");
            dt.Columns.Add("StatusName");

            dt.Columns.Add("Title");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int pid = int.Parse(dt.Rows[i]["ID"].ToString());
                string strStatusName = "正在审批";
                Yan_WF_TD TD = new Yan_WF_PIB().GetPDStatus(pid);

                dt.Rows[i]["TaskName"] = TD.TaskName;
                dt.Rows[i]["TaskAssInfo"] = TD.TaskAssInfo;
                dt.Rows[i]["Taskorder"] = TD.Taskorder;
                dt.Rows[i]["Title"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjInput21362");
                if (dt.Rows[i]["isComplete"].ToString() == "Y")
                {
                    strStatusName = "已审批";
                }
                if (dt.Rows[i]["IsCanceled"].ToString() == "Y")
                {
                    strStatusName = "已退回";
                    dt.Rows[i]["TaskName"] = TD.TaskName + "【已退回】";

                }
                dt.Rows[i]["StatusName"] = strStatusName;

            }
            if (P2 == "0")
            {
                //所有题案
                msg.Result = dt;
            }
            if (P2 == "1")
            {
                //已立案
                msg.Result = dt.Where(" Taskorder='4'  and  StatusName='正在审批' ");
            }
            if (P2 == "2")
            {
                //未立案
                msg.Result = dt.Where(" StatusName='已退回'");
            }
            if (P2 == "3")
            {
                //未立案
                msg.Result = dt.Where(" StatusName='已审批'");
            }
        }





        /// <summary>
        /// 获取待审核的提案数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETDCLDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = UserInfo.User.UserName;
            string strType = P2;
            List<string> intProD = new Yan_WF_PIB().GetDSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();

            if (intProD.Count > 0)
            {

                string strWhere = " AND  Yan_WF_PI.ID IN (" + intProD.ListTOString(',') + ")";
                string strSQL = " SELECT Yan_WF_PI.*,Yan_WF_PD.ProcessName,Yan_WF_PD.ProcessClass,Yan_WF_PD.ProcessType FROM  Yan_WF_PI  INNER JOIN Yan_WF_PD ON Yan_WF_PI.PDID =Yan_WF_PD.ID  WHERE PDID='257'  " + strWhere + "  ORDER BY Yan_WF_PI.ID DESC"; ;
                DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
                dt.Columns.Add("TaskName");
                dt.Columns.Add("TaskAssInfo");
                dt.Columns.Add("Taskorder");
                dt.Columns.Add("StatusName");

                dt.Columns.Add("Title");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int pid = int.Parse(dt.Rows[i]["ID"].ToString());
                    string strStatusName = "正在审批";
                    Yan_WF_TD TD = new Yan_WF_PIB().GetPDStatus(pid);
                    if (dt.Rows[i]["isComplete"].ToString() == "Y")
                    {
                        strStatusName = "已审批";
                    }
                    if (dt.Rows[i]["IsCanceled"].ToString() == "Y")
                    {
                        strStatusName = "已退回";
                    }
                    dt.Rows[i]["StatusName"] = strStatusName;
                    dt.Rows[i]["TaskName"] = TD.TaskName;
                    dt.Rows[i]["TaskAssInfo"] = TD.TaskAssInfo;
                    dt.Rows[i]["Taskorder"] = TD.Taskorder;
                    dt.Rows[i]["Title"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjInput21362");
                }

                //待审核
                DataTable dtDFY = dt.Where(" Taskorder='2'  and  StatusName='正在审批' ");
                DataTable dtDSH = dt.Where(" ( Taskorder='3'or Taskorder='6' ) and  StatusName='正在审批' ");
                DataTable dtDBL = dt.Where(" Taskorder='4'  and StatusName='正在审批' ");
                DataTable dtDPJ = dt.Where(" Taskorder='5'  and StatusName='正在审批' ");
                DataTable dtDFS = dt.Where(" Taskorder='7'  and StatusName='正在审批' ");
                if (P2 == "0")
                {
                    msg.Result = dtDSH;
                }
                if (P2 == "1")
                {
                    //待办理
                    msg.Result = dtDBL;
                }
                if (P2 == "2")
                {
                    //待评价
                    msg.Result = dtDPJ;
                }
                if (P2 == "3")
                {
                    //待归档
                    msg.Result = dtDFS;
                }
                if (P2 == "4")
                {
                    //待附议
                    msg.Result = dtDFY;
                }
                msg.Result1 = dtDSH.Rows.Count + "," + dtDBL.Rows.Count + "," + dtDPJ.Rows.Count + "," + dtDFS.Rows.Count + "," + dtDFY.Rows.Count;

                msg.Result2 = new Yan_WF_PIB().GetDTByCommand("SELECT COUNT(*) AS thCount FROM  yan_wf_pi WHERE IsCanceled='Y' AND CRUser='" + UserInfo.User.UserName + "'").Rows[0][0].ToString();
            }
        }


        public void GETYCLDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = UserInfo.User.UserName;
            string strType = P2;
            List<string> intProD = new Yan_WF_PIB().GetYSH(UserInfo.User).Select(d => d.PIID.ToString()).ToList();

            if (intProD.Count > 0)
            {

                string strWhere = " AND  Yan_WF_PI.ID IN (" + intProD.ListTOString(',') + ")";
                string strSQL = " SELECT Yan_WF_PI.*,Yan_WF_PD.ProcessName,Yan_WF_PD.ProcessClass,Yan_WF_PD.ProcessType FROM  Yan_WF_PI  INNER JOIN Yan_WF_PD ON Yan_WF_PI.PDID =Yan_WF_PD.ID  WHERE PDID='257'  " + strWhere + "  ORDER BY Yan_WF_PI.ID DESC"; ;
                DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
                dt.Columns.Add("TaskName");
                dt.Columns.Add("TaskAssInfo");
                dt.Columns.Add("Taskorder");
                dt.Columns.Add("StatusName");

                dt.Columns.Add("Title");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int pid = int.Parse(dt.Rows[i]["ID"].ToString());
                    string strStatusName = "正在审批";
                    Yan_WF_TD TD = new Yan_WF_PIB().GetPDStatus(pid);
                    if (dt.Rows[i]["isComplete"].ToString() == "Y")
                    {
                        strStatusName = "已审批";
                    }
                    if (dt.Rows[i]["IsCanceled"].ToString() == "Y")
                    {
                        strStatusName = "已退回";
                    }
                    dt.Rows[i]["StatusName"] = strStatusName;
                    dt.Rows[i]["TaskName"] = TD.TaskName;
                    dt.Rows[i]["TaskAssInfo"] = TD.TaskAssInfo;
                    dt.Rows[i]["Taskorder"] = TD.Taskorder;
                    dt.Rows[i]["Title"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjInput21362");
                }
                msg.Result = dt;

            }
        }


        public void GETBLZDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {


            string strSQL = "SELECT Yan_WF_PI.ID,Yan_WF_PI.IsCanceled,Yan_WF_PI.CRDate,Yan_WF_PI.isComplete,Yan_WF_PI.CRUserName,Yan_WF_PI.BranchName,Yan_WF_PI.ID, Yan_WF_PD.ProcessName,Yan_WF_PD.ProcessClass,Yan_WF_PD.ProcessType FROM  Yan_WF_PI  INNER JOIN Yan_WF_PD ON Yan_WF_PI.PDID =Yan_WF_PD.ID  WHERE PDID='257'  AND  Yan_WF_PI.isComplete IS NULL  ORDER BY Yan_WF_PI.ID DESC";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            dt.Columns.Add("TaskName");
            dt.Columns.Add("TaskAssInfo");
            dt.Columns.Add("Taskorder");
            dt.Columns.Add("StatusName");

            dt.Columns.Add("Title");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                int pid = int.Parse(dt.Rows[i]["ID"].ToString());

                Yan_WF_PI PI = new Yan_WF_PIB().GetEntity(D => D.ID == pid);
                string strStatusName = "正在审批";
                Yan_WF_TD TD = new Yan_WF_PIB().GetPDStatus(pid);
                if (dt.Rows[i]["isComplete"].ToString() == "Y")
                {
                    strStatusName = "已审批";
                }
                if (dt.Rows[i]["IsCanceled"].ToString() == "Y")
                {
                    strStatusName = "已退回";
                }
                dt.Rows[i]["StatusName"] = strStatusName;
                dt.Rows[i]["TaskName"] = TD.TaskName;
                dt.Rows[i]["TaskAssInfo"] = TD.TaskAssInfo;
                dt.Rows[i]["Taskorder"] = TD.Taskorder;

                dt.Rows[i]["Title"] = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput21362");
            }
            msg.Result = dt;


        }


        /// <summary>
        /// 同步相关人员手机号
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBSJH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string strSQL = "SELECT DISTINCT jh_auth_userrole.UserName,mobphone FROM jh_auth_userrole LEFT  JOIN jh_auth_user ON jh_auth_userrole.UserName=jh_auth_user.UserName WHERE RoleCode IN ('1272','1273','1274','1275','1276','1277')";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            string posturl = "http://202.195.235.166:8220/mdcs/rest/getDataInfo";

            //jb.Add("paramString", "0x7b224944223a22303032323036227d");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                JObject jb = new JObject();

                jb.Add("page", 1);
                jb.Add("pagesize", 10);
                String strUserName = dt.Rows[i]["UserName"].ToString();
                jb.Add("paramString", "0x" + StrToHex("{\"ID\":\"" + strUserName + "\"}").Replace(" ", ""));
                string re = PostWebRequest(posturl, JsonConvert.SerializeObject(jb), Encoding.UTF8);
                JObject ret = JObject.Parse(re);

                JObject sjdata = (JObject)ret["data"];

                JArray prlist = (JArray)sjdata["Rows"];

                foreach (var item in prlist)
                {
                    string sjh = (string)item["SJHM"];

                    if (!string.IsNullOrEmpty(sjh))
                    {
                        new Yan_WF_PIB().ExsSclarSql(" UPDATE jh_auth_user SET mobphone='" + sjh + "' WHERE UserName='" + strUserName + "' ");
                    }
                }




            }
        }
        public static string PostWebRequest(string postUrl, string paramData, Encoding dataEncode)
        {
            string ret = string.Empty;
            try
            {
                byte[] byteArray = dataEncode.GetBytes(paramData); //转化
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(new Uri(postUrl));
                webReq.Method = "POST";
                webReq.ContentType = "application/json; charset=UTF-8";
                webReq.Headers.Add("int_num", "b43e6b6351c54f2f87c9e3fc25efa49f");
                webReq.Headers.Add("applyId", "V_TRA_TEACHERS_ALL");
                webReq.Headers.Add("accessToken", "B00EA43933814695E053ABEBC3CA78BC");

                webReq.ContentLength = byteArray.Length;
                Stream newStream = webReq.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                ret = sr.ReadToEnd();
                sr.Close();
                response.Close();
                newStream.Close();
            }
            catch (Exception ex)
            {
                CommonHelp.WriteLOG(ex.ToString());
                return ex.Message;
            }
            return ret;
        }

        public string StrToHex(string mStr) //返回处理后的十六进制字符串
        {
            return BitConverter.ToString(
            ASCIIEncoding.Default.GetBytes(mStr)).Replace("-", " ");
        } /* StrToHex */

        /// <summary>
        /// 获取要发送的短信
        /// </summary>
        /// <param name="MsgType"></param>
        /// <param name="strPID"></param>
        /// <returns></returns>
        public static string GetMsg(string MsgType, Yan_WF_PI PI) //返回处理后的十六进制字符串
        {
            string strTAName = new Yan_WF_PIB().GetFiledVal(PI.Content, "qjInput21362");
            string strMsg = "";
            if (MsgType == "0")
            {
                strMsg = "确认提醒:关于“" + strTAName + "”需要您的附议，请您尽快确认，谢谢！";
            }
            if (MsgType == "1")
            {
                strMsg = "提交提醒:关于“" + strTAName + "”已提交，请您审核，谢谢！";
            }
            if (MsgType == "2")
            {
                strMsg = "退回提醒:关于“" + strTAName + "提案”被退回，经审核不能予以立案，请谅解！";
            }
            if (MsgType == "3")
            {
                strMsg = "审批提醒：您有一件针对贵部门的教代会提案，敬请关注并及时回复，谢谢！";
            }
            if (MsgType == "4")
            {
                strMsg = "提交提醒:关于“" + strTAName + "提案”已答复，请您及时评价，谢谢！";
            }
            if (MsgType == "5")
            {
                strMsg = "提醒:关于“" + strTAName + "提案”被评价为“不满意”，请您关注！";
            }
            if (MsgType == "6")
            {
                strMsg = "有一件针对您分管部门的教代会提案，部门已回复，敬请您审阅，谢谢！";
            }
            if (MsgType == "7")
            {
                strMsg = "归档提醒：关于“" + strTAName + "提案”校领导已审阅，请查收。";
            }
            return strMsg;
        } /* StrToHex */







        /// <summary>
        /// 获取公示列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGSTADATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strUserName = UserInfo.User.UserName;
            string strType = P2;



            string strSQL = "  SELECT Yan_WF_PI.ID,Yan_WF_PI.CRUserName,Yan_WF_PI.CRDate,Yan_WF_PI.Content,Yan_WF_PI.isComplete,Yan_WF_PI.IsCanceled,Yan_WF_PI.IsCanceled,ISNULL(Yan_WF_PI.Remark1, '0') AS djl   FROM  Yan_WF_PI  INNER JOIN Yan_WF_PD ON Yan_WF_PI.PDID =Yan_WF_PD.ID WHERE PDID = '257'  AND Yan_WF_PI.iscomplete is NULL and Yan_WF_PI.iscanceled is NULL ORDER BY Yan_WF_PI.ID DESC";
            DataTable dt = new Yan_WF_PIB().GetDTByCommand(strSQL);
            dt.Columns.Add("TaskName");
            dt.Columns.Add("TaskAssInfo");
            dt.Columns.Add("Taskorder");
            dt.Columns.Add("StatusName");
            dt.Columns.Add("Title");
            dt.Columns.Add("tafyrname");
            dt.Columns.Add("talb");
            dt.Columns.Add("isla");
            dt.Columns.Add("zbdw");
            dt.Columns.Add("xbdw");
            dt.Columns.Add("myd");
            dt.Columns.Add("tabh");
            dt.Columns.Add("tawyj");





            msg.Result2 = dt.Rows.Count;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int pid = int.Parse(dt.Rows[i]["ID"].ToString());
                string strStatusName = "正在审批";
                Yan_WF_TD TD = new Yan_WF_PIB().GetPDStatus(pid);

                dt.Rows[i]["TaskName"] = TD.TaskName;
                dt.Rows[i]["TaskAssInfo"] = TD.TaskAssInfo;
                dt.Rows[i]["Taskorder"] = TD.Taskorder;

                dt.Rows[i]["Title"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjInput21362");
                dt.Rows[i]["tafyrname"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjSelect31533", "1");
                dt.Rows[i]["talb"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjSelect22928");
                dt.Rows[i]["zbdw"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjSelect63401", "1");
                dt.Rows[i]["xbdw"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjSelect63401", "1");

                dt.Rows[i]["tabh"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjSN28396");
                dt.Rows[i]["tawyj"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjInput16720");

                if (TD.Taskorder > 3)
                {
                    dt.Rows[i]["isla"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjCheck34675");

                }
                if (TD.Taskorder > 5)
                {
                    dt.Rows[i]["myd"] = new Yan_WF_PIB().GetFiledVal(dt.Rows[i]["Content"].ToString(), "qjCheck56971");
                }

                dt.Rows[i]["CRDate"] = DateTime.Now.ToString("yyyy-MM-dd");



                if (dt.Rows[i]["isComplete"].ToString() == "Y")
                {
                    strStatusName = "已审批";
                }
                if (dt.Rows[i]["IsCanceled"].ToString() == "Y")
                {
                    strStatusName = "已退回";
                    dt.Rows[i]["TaskName"] = TD.TaskName + "【已退回】";

                }
                dt.Rows[i]["StatusName"] = strStatusName;

            }

            string strComSQL = "select qj_tian.*, ISNULL(yan_wf_pi.Remark1, '0') AS djl  from qj_tian  INNER JOIN yan_wf_pi ON qj_tian.intProcessStanceid=yan_wf_pi.ID where isla='同意立案'";
            DataTable dtCom = new Yan_WF_PIB().GetDTByCommand(strComSQL);


            for (int i = 0; i < dtCom.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr["ID"] = dtCom.Rows[i]["intProcessStanceid"].ToString(); //
                dr["Title"] = dtCom.Rows[i]["tatitle"].ToString();
                dr["tafyrname"] = dtCom.Rows[i]["tafyrname"].ToString();
                dr["talb"] = dtCom.Rows[i]["talb"].ToString();
                dr["zbdw"] = dtCom.Rows[i]["zbdw"].ToString();
                dr["xbdw"] = dtCom.Rows[i]["xbdw"].ToString();
                dr["myd"] = dtCom.Rows[i]["myd"].ToString();
                dr["isla"] = dtCom.Rows[i]["isla"].ToString();
                dr["tabh"] = dtCom.Rows[i]["tabh"].ToString();
                dr["tawyj"] = dtCom.Rows[i]["tawyj"].ToString();
                dr["TaskName"] = "已办理";
                dr["CRUserName"] = dtCom.Rows[i]["CRUserName"].ToString();
                dr["CRDate"] = dtCom.Rows[i]["CRDate"].ToString();
                dr["djl"] = dtCom.Rows[i]["djl"].ToString();

                dt.Rows.Add(dr);
            }


            msg.Result = dt;
            msg.Result1 = dt.Rows.Count;
            msg.Result2 = dtCom.Rows.Count;
            msg.Result3 = dt.Rows.Count - dtCom.Rows.Count;



        }

        #endregion





    }
}