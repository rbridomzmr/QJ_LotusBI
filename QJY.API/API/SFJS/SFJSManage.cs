﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace QJY.API
{
    public class SFJSManage
    {

        #region PC端


        //获取短信内容
        public void DRJCDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            DataView dataView = dtalldata.DefaultView;

            string strZID = P2;
            string strZName = getZinfo(P2);
            #region 处理县市
            DataTable dtxq = dataView.ToTable(true, "县市");
            for (int i = 0; i < dtxq.Rows.Count; i++)
            {
                string strXSName = dtxq.Rows[i][0].ToString();
                DataTable dtTempxs = isExitXS(strXSName, strZID);
                if (dtTempxs.Rows.Count == 0)
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '县市', '" + strXSName + "', '0', '" + strZID + "', '', '" + strZID + "', 'administrator', '" + strZName + "', '0', '" + strZName + "');");
                }

            }
            #endregion
            #region 处理乡镇
            DataTable dtxz = dataView.ToTable(true, "县市", "乡镇");
            for (int i = 0; i < dtxz.Rows.Count; i++)
            {
                string strXSName = dtxz.Rows[i][0].ToString();
                string strXZName = dtxz.Rows[i][1].ToString();
                DataTable dtTempxs = isExitXS(strXSName, strZID);
                DataTable dtTempxz = isExitXZ(strXSName, strXZName, strZID);
                if (dtTempxz.Rows.Count == 0)
                {
                    string pid = dtTempxs.Rows[0]["ID"].ToString();
                    string pname = dtTempxs.Rows[0]["czname"].ToString();
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '乡镇', '" + strXZName + "', '" + pid + "', '" + strZID + "', '" + pname + "', '" + strZID + "', 'administrator', '" + strZName + "', '1', '" + strZName + "');");
                }
            }
            #endregion
            #region 处理村
            DataTable dtc = dataView.ToTable(true, "县市", "乡镇", "村");
            for (int i = 0; i < dtc.Rows.Count; i++)
            {
                string strXSName = dtc.Rows[i][0].ToString();
                string strXZName = dtc.Rows[i][1].ToString();
                string strCName = dtc.Rows[i][2].ToString();
                DataTable dtTempxz = isExitXZ(strXSName, strXZName, strZID);
                DataTable dtTempC = isExitC(strXSName, strXZName, strCName, strZID);
                if (dtTempC.Rows.Count == 0)
                {
                    string pid = dtTempxz.Rows[0]["ID"].ToString();
                    string pname = dtTempxz.Rows[0]["pname"].ToString() + "/" + dtTempxz.Rows[0]["czname"].ToString();
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_xzq (CRUser, CRDate, intProcessStanceid, ComID, type, czname, pid, zid, pname, DCode, CRUserName, zname, level, DName) VALUES ('administrator', '2020-07-24 17:03:40', '0', '10334', '村', '" + strCName + "', '" + pid + "', '" + strZID + "', '" + pname + "', '" + strZID + "', 'administrator', '" + strZName + "', '2', '" + strZName + "');");
                }
            }
            #endregion

            #region 处理斗渠
            DataTable dtdq = dataView.ToTable(true, "干支渠", "段别", "斗渠");
            for (int i = 0; i < dtdq.Rows.Count; i++)
            {
                string strGQName = dtdq.Rows[i][0].ToString();
                string strDName = dtdq.Rows[i][1].ToString();
                string strDQName = dtdq.Rows[i][2].ToString();

                if (isExitDB(strDQName, strGQName, strZID))
                {
                    new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_db (CRUser, CRDate, intProcessStanceid, ComID, zid, dbname, gzqname, dqname, remark, DCode, DName, CRUserName, zname, did) VALUES ( 'administrator', '2020-07-24 16:55:09', '0', '10334', '" + strZID + "', '" + strDQName + "', '" + strGQName + "', '" + strDName + "', NULL, '" + strZID + "', '" + strZName + "', 'administrator', '" + strZName + "', NULL);");
                }

            }
            #endregion







            //DataView dataView = dt.DefaultView;
            //DataTable dtxq = dataView.ToTable(true, "乡镇");
            //DataTable dtc = dataView.ToTable(true, "村");//注：其中ToTable（）的第一个参数为是否DISTINCT
        }


        public void JCDRDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            List<string> Listcname = new List<string>() { "县市", "乡镇", "村", "组", "干支渠", "斗渠", "段别", "姓名", "身份证号", "手机号码", "粮油作物面积", "经济作物面积" };
            string strMsg = "";
            foreach (string cname in Listcname)
            {
                if (!dtalldata.Columns.Contains(cname))
                {
                    strMsg = strMsg + cname + "列缺少,";
                }

            }
            msg.ErrorMsg = strMsg;


        }

        public void DRUSERDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = JsonConvert.DeserializeObject<DataTable>(P1);
            DataView dataView = dtalldata.DefaultView;
            string strZID = P2;
            string strZName = getZinfo(P2);
            string strMsg = "出错信息:";
            #region 处理小组
            DataTable dtcz = dataView.ToTable(true, "乡镇", "村", "组", "干支渠", "斗渠");
            for (int i = 0; i < dtcz.Rows.Count; i++)
            {
                try
                {


                    string strXZName = dtcz.Rows[i][0].ToString();
                    string strCName = dtcz.Rows[i][1].ToString();
                    string strCZName = dtcz.Rows[i][2].ToString();
                    string strGQName = dtcz.Rows[i][3].ToString();
                    string strDName = dtcz.Rows[i][4].ToString();
                    if (isExitCZ(strCZName, strXZName + "/" + strCName, strGQName + "/" + strDName, strZID).Rows.Count == 0)
                    {
                        DataTable dtD = getDinfo(strGQName, strDName, strZID);
                        DataTable dttempxz = getXZinfo(strXZName, strCName, strZID);


                        string DID = dtD.Rows[0]["ID"].ToString();
                        string xzname = dttempxz.Rows[0]["pname"].ToString() + "/" + dttempxz.Rows[0]["czname"].ToString();
                        string xzid = dttempxz.Rows[0]["ID"].ToString();

                        new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_cz ( CRUser, DCode, DName, CRUserName, CRDate, intProcessStanceid, ComID, zid, zname, xzname, pid, pname, did, dbname) VALUES ('administrator', '" + strZID + "', '" + strZName + "', 'administrator', '2020-07-25 22:36:06', '0', '10334', '" + strZID + "', '" + strZName + "', '" + strCZName + "', '" + xzid + "', '" + xzname + "', '" + DID + "', '" + strGQName + "/" + strDName + "');");
                    }
                }
                catch (Exception)
                {

                    strMsg = strMsg + "小组" + i.ToString();
                }
            }
            #endregion

            #region 处理缴费用户
            for (int i = 0; i < dtalldata.Rows.Count; i++)
            {
                try
                {
                    string strXSName = dtalldata.Rows[i]["县市"].ToString();
                    string strXZName = dtalldata.Rows[i]["乡镇"].ToString();
                    string strCName = dtalldata.Rows[i]["村"].ToString();
                    string strCZName = dtalldata.Rows[i]["组"].ToString();

                    string strGQName = dtalldata.Rows[i]["干支渠"].ToString();
                    string strDName = dtalldata.Rows[i]["斗渠"].ToString();
                    string strDBName = dtalldata.Rows[i]["段别"].ToString();

                    string strXMe = dtalldata.Rows[i]["姓名"].ToString();
                    string strSFZH = dtalldata.Rows[i]["身份证号"].ToString();
                    string strSJHM = dtalldata.Rows[i]["手机号码"].ToString();

                    string strLYMJ = dtalldata.Rows[i]["粮油作物面积"].ToString();
                    string strJJMJ = dtalldata.Rows[i]["经济作物面积"].ToString();
                    string strZWMJ = "0";
                    if (isExitXM(strSFZH, strCZName, strZID).Rows.Count == 0)
                    {
                        DataTable dttempxz = getCzinfo(strXSName + "/" + strXZName + "/" + strCName, strGQName + "/" + strDName, strCZName, strZID);
                        string xzid = dttempxz.Rows[0]["ID"].ToString();
                        string xzNAME = dttempxz.Rows[0]["xzname"].ToString();

                        new JH_Auth_BranchB().ExsSclarSql("INSERT INTO qj_sfjs_jfuser ( CRUser, DCode, DName, CRUserName, CRDate, intProcessStanceid, ComID, YHName, zid, zname, sfzbm, lxdh, xzid, xzname, dlpassword,lsmj, jjmj, zmj) VALUES ( 'administrator', '" + strZID + "', '" + strZName + "', 'administrator', '2020-07-28 11:38:11', '0', '10334', '" + strXMe + "', '" + strZID + "', '" + strZName + "', '" + strSFZH + "', '" + strSJHM + "', '" + xzid + "', '" + xzNAME + "', '123456', '" + strLYMJ + "', '" + strJJMJ + "', '" + strZWMJ + "');");
                    }
                }
                catch (Exception)
                {

                    strMsg = strMsg + "用户" + i.ToString();
                }
            }

            msg.Result1 = strMsg;
            #endregion
        }


        public void CLDRDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strZID = P1;

            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz ");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string intPID = dt.Rows[i]["pid"].ToString();
                string intid = dt.Rows[i]["ID"].ToString();
                DataTable dtxzq = new JH_Auth_BranchB().GetDTByCommand("SELECT  (PNAME +'/' +CZNAME) AS XZNAME  FROM qj_xzq WHERE  id ='" + intPID + "'");
                new JH_Auth_BranchB().ExsSclarSql("UPDATE qj_cz SET pname='" + dtxzq.Rows[0]["XZNAME"].ToString() + "' WHERE ID='" + intid + "'");
            }


        }




        public void DelZData(string zid)
        {
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_xzuser WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_xzq WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_sfjs_jfuser WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_db WHERE ZID='" + zid + "'");
            new JH_Auth_BranchB().ExsSclarSql("DELETE FROM qj_cz WHERE ZID='" + zid + "'");

        }
        public DataTable ToDataTable(string json)
        {
            DataTable dataTable = new DataTable();  //实例化
            DataTable result;
            try
            {

                ArrayList arrayList = JsonConvert.DeserializeObject<ArrayList>(json);
                if (arrayList.Count > 0)
                {
                    foreach (Dictionary<string, object> dictionary in arrayList)
                    {
                        if (dictionary.Keys.Count == 0)
                        {
                            result = dataTable;
                            return result;
                        }
                        if (dataTable.Columns.Count == 0)
                        {
                            foreach (string current in dictionary.Keys)
                            {
                                dataTable.Columns.Add(current, dictionary[current].GetType());
                            }
                        }
                        DataRow dataRow = dataTable.NewRow();
                        foreach (string current in dictionary.Keys)
                        {
                            dataRow[current] = dictionary[current];
                        }

                        dataTable.Rows.Add(dataRow); //循环添加行到DataTable中
                    }
                }
            }
            catch
            {
            }
            result = dataTable;
            return result;
        }


        public string getZinfo(string strZid)
        {
            string strZinfo = "";
            JH_Auth_Branch branch = new JH_Auth_BranchB().GetBMByDeptCode(10334, int.Parse(strZid));
            strZinfo = branch.DeptName;
            if (branch.DeptRoot != 1)
            {
                JH_Auth_Branch Pbranch = new JH_Auth_BranchB().GetBMByDeptCode(10334, branch.DeptRoot);

                strZinfo = Pbranch.DeptName + "/" + strZinfo;
            }

            return strZinfo;
        }


        public DataTable getDinfo(string strZQ, string strDQ, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_db WHERE (dbname='" + strDQ + "' OR dbname='" + strDQ + "斗') AND gzqname='" + strZQ + "' AND  zid='" + strZid + "'");
            return dt;
        }
        public DataTable getCzinfo(string strXZQ, string strDQ, string strxzname, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz WHERE ZID='" + strZid + "'AND xzname='" + strxzname + "' AND pname='" + strXZQ + "' AND dbname='" + strDQ + "'");
            return dt;
        }

        public DataTable getXZinfo(string strXZ, string strC, string strZid)
        {
            DataTable dt = new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE czname='" + strC + "' AND pname like '%" + strXZ + "%' AND  zid='" + strZid + "'");
            return dt;
        }

        public bool isExitDB(string strDQ, string strZQ, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_db WHERE dbname='" + strDQ + "' AND gzqname='" + strZQ + "' AND  zid='" + ZID + "'").Rows.Count == 0;
        }



        public DataTable isExitXS(string strXSName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strXSName + "' AND type='县市' AND  zid='" + ZID + "'");
        }


        public DataTable isExitXZ(string strXSName, string strXZName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strXZName + "' AND pname='" + strXSName + "' AND type='乡镇' AND  zid='" + ZID + "'");
        }


        public DataTable isExitC(string strXSName, string strXZName, string strCName, string ZID)
        {
            string strXX = strXSName + "/" + strXZName;
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_xzq WHERE CZname='" + strCName + "' AND pname='" + strXX + "' AND type='村' AND  zid='" + ZID + "'");
        }

        public DataTable isExitCZ(string strCZName, string strXZQName, string strDBName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_cz WHERE xzname='" + strCZName + "' AND dbname='" + strDBName + "' AND pname LIKE '%" + strXZQName + "' AND  zid='" + ZID + "'");
        }


        public DataTable isExitXM(string strSFZHM, string strXZQName, string ZID)
        {
            return new JH_Auth_BranchB().GetDTByCommand("SELECT * FROM qj_sfjs_jfuser WHERE sfzbm='" + strSFZHM + "' AND  zid='" + ZID + "'");
        }

        public void GETGLZINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtalldata = new DataTable();
            dtalldata = new JH_Auth_BranchB().GetDTByCommand("SELECT deptcode as id,DEPTNAME AS label,deptroot as pid  FROM jh_auth_branch WHERE DEPTCODE IN ('" + UserInfo.UserBMQXCode.ToFormatLike() + "') ");
            msg.Result = dtalldata;

        }



        #endregion



        #region andorid端


        /// <summary>
        /// 获取新闻详情
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETAPPSYDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dt = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM qj_sys_tzgg WHERE id='" + P1 + "'");
            msg.Result = dt;
        }

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETXWLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtxw = new JH_Auth_QYB().GetDTByCommand("SELECT id,title,fbdate,imgurl,lls FROM qj_sys_tzgg  ORDER BY ID DESC");
            msg.Result = dtxw;



            #region 生成测试数据
            //DBFactory db = new BI_DB_SourceB().GetDB(0);
            //DataTable dtSB = new JH_Auth_QYB().GetDTByCommand(" SELECT * FROM jh_sbdata");
            //DataTable dtYH = new JH_Auth_QYB().GetDTByCommand("SELECT qj_sfjs_jfuser.*,qj_cz.did,qj_cz.dbname,qj_cz.pname,qj_cz.pid FROM qj_sfjs_jfuser INNER  JOIN qj_cz  ON qj_sfjs_jfuser.xzid=qj_cz.ID WHERE qj_sfjs_jfuser.ZID='4113' AND xzid IN ('124','125','126','127')");

            //for (int i = 0; i < dtSB.Rows.Count; i++)
            //{

            //    try
            //    {
            //        Random ran = new Random();
            //        int RandKey = ran.Next(1, 190);

            //        var dt = new Dictionary<string, object>();
            //        dt.Add("DE", dtSB.Rows[i]["DE"].ToString());
            //        dt.Add("YSSC", dtSB.Rows[i]["YSSC"].ToString());
            //        dt.Add("CZNAME", dtYH.Rows[RandKey]["xzname"].ToString());

            //        dt.Add("TDMS", dtSB.Rows[i]["TDMS"].ToString());
            //        dt.Add("SLL", dtSB.Rows[i]["SLL"].ToString());
            //        dt.Add("ZJE", dtSB.Rows[i]["ZJE"].ToString());
            //        dt.Add("ZID", dtYH.Rows[RandKey]["zid"].ToString());
            //        dt.Add("XZID", dtYH.Rows[RandKey]["xzid"].ToString());
            //        dt.Add("XM", dtYH.Rows[RandKey]["YHName"].ToString());
            //        dt.Add("SL2", dtSB.Rows[i]["SL2"].ToString());
            //        dt.Add("SL1", dtSB.Rows[i]["SL1"].ToString());
            //        dt.Add("CRUser", "20200801001");
            //        dt.Add("CRDate", dtSB.Rows[i]["CRDate"].ToString());
            //        dt.Add("TYPE", dtSB.Rows[i]["TYPE"].ToString());

            //        dt.Add("DB", dtYH.Rows[RandKey]["dbname"].ToString());
            //        dt.Add("DW2", dtSB.Rows[i]["DW2"].ToString());
            //        dt.Add("DID", dtYH.Rows[RandKey]["did"].ToString());
            //        dt.Add("DW1", dtSB.Rows[i]["DW1"].ToString());
            //        dt.Add("CID", dtYH.Rows[RandKey]["pid"].ToString());
            //        dt.Add("cname", dtYH.Rows[RandKey]["pname"].ToString());


            //        dt.Add("sfzhm", dtYH.Rows[RandKey]["sfzbm"].ToString());
            //        dt.Add("zname", dtYH.Rows[RandKey]["zname"].ToString());



            //        dt.Add("intProcessStanceid", "0");
            //        dt.Add("ComID", "10334");
            //        dt.Add("GYSJ", dtSB.Rows[i]["GYSJ"].ToString());
            //        dt.Add("GYZJE", dtSB.Rows[i]["GYZJE"].ToString());
            //        db.InserData(dt, "qj_sfjs_ysdata");
            //    }
            //    catch (Exception)
            //    {
            //        continue;
            //    }



            //}
            #endregion
        }




        /// <summary>
        /// 修改密码接口
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>   
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void XGMM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP);
            if (dtUser.Rows.Count > 0)
            {
                if (dtUser.Rows[0]["usertype"].ToString() == "村组管理员")
                {
                    new JH_Auth_QYB().ExsSclarSql(" UPDATE qj_xzuser set password='" + P1 + "' WHERE czusername ='" + dtUser.Rows[0]["username"].ToString() + "'");
                }
                else
                {
                    new JH_Auth_QYB().ExsSclarSql(" UPDATE jh_auth_user set userpass='" + CommonHelp.GetMD5(P1) + "' WHERE username ='" + dtUser.Rows[0]["username"].ToString() + "'");
                }

            }

        }


        /// <summary>
        /// 获取管理站信息接口(包含价格,定额数据)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETZINFO(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP);
            DataTable dtdeinfo = new DataTable();
            DataTable dtjg = new DataTable();
            DataTable dtjq = new DataTable();

            string strZid = P1;
            if (dtUser.Rows.Count > 0)
            {
                dtdeinfo = new JH_Auth_QYB().GetDTByCommand("SELECT  dymc as type,dw,zid,type as zwlb  FROM qj_sfjs_de where zid='" + strZid + "' ");
                dtjg = new JH_Auth_QYB().GetDTByCommand("SELECT TYPE,DW,JG,GYJG,JQMC FROM qj_sfjs_jg  where zid ='" + strZid + "'");
                dtjq = new JH_Auth_QYB().GetDTByCommand("SELECT DISTINCT JQMC  FROM qj_sfjs_jg  where zid ='" + strZid + "'  ORDER BY jqmc DESC");

                dtjq.Columns.Add("JGS", Type.GetType("System.Object"));
                for (int i = 0; i < dtjq.Rows.Count; i++)
                {
                    string strJQMC = dtjq.Rows[i]["JQMC"].ToString();
                    dtjq.Rows[i]["JGS"] = dtjg.Where(" JQMC = '" + strJQMC + "'");
                }
            }
            msg.Result = dtdeinfo;
            msg.Result1 = dtjg;
            msg.Result2 = dtjq;


        }



        /// <summary>
        /// 根据小组,获取对应村组得灌溉用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETGGYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            string index = context.Request("INDEX") ?? "1";
            DataTable dtUser = GetUserInfo(UP);
            DataTable dtyhinfo = new DataTable();
            string strxzid = P1;
            string strWhere = "";
            if (!string.IsNullOrEmpty(P2))
            {
                strWhere = "AND ( YHName LIKE '%" + P2 + "%' OR sfzbm   LIKE  '%" + P2 + "%')";
            }

            if (dtUser.Rows.Count > 0)
            {
                if (index == "0")
                {
                    dtyhinfo = new JH_Auth_QYB().Db.SqlQueryable<Object>(" SELECT qj_sfjs_jfuser.xzid,YHName,qj_sfjs_jfuser.zid,qj_sfjs_jfuser.zname,sfzbm,lxdh,qj_sfjs_jfuser.xzname,qj_sfjs_jfuser.ID,qj_cz.dbname,qj_cz.pname FROM qj_sfjs_jfuser INNER JOIN qj_cz ON qj_sfjs_jfuser.xzid=qj_cz.ID  WHERE  XZID  IN ('" + strxzid.ToFormatLike() + "')  " + strWhere).OrderBy(" ID DESC").ToDataTable();
                }
                else
                {
                    dtyhinfo = new JH_Auth_QYB().Db.SqlQueryable<Object>(" SELECT qj_sfjs_jfuser.xzid,YHName,qj_sfjs_jfuser.zid,qj_sfjs_jfuser.zname,sfzbm,lxdh,qj_sfjs_jfuser.xzname,qj_sfjs_jfuser.ID,qj_cz.dbname,qj_cz.pname FROM qj_sfjs_jfuser INNER JOIN qj_cz ON qj_sfjs_jfuser.xzid=qj_cz.ID  WHERE  XZID  IN ('" + strxzid.ToFormatLike() + "')  " + strWhere).OrderBy(" ID DESC").ToDataTablePage(int.Parse(index), 20);

                }
            }

            for (int i = 0; i < dtyhinfo.Rows.Count; i++)
            {
                dtyhinfo.Rows[i]["xzname"] = dtyhinfo.Rows[i]["pname"].ToString() + "/" + dtyhinfo.Rows[i]["xzname"].ToString();
            }
            msg.Result = dtyhinfo;

        }

        public void SBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            //CommonHelp.WriteLOG(P1);

            DataTable dtUser = GetUserInfo(UP);


            if (string.IsNullOrEmpty(context.Request("XM")))
            {
                msg.ErrorMsg = "姓名不能为空";
                return;
            }
            //if (Branch.Remark2 == "N")
            //{
            //    msg.ErrorMsg = "当前数据上报功能处于关闭状态!";
            //    return;
            //}



            string strXMSFZ = context.Request("XM");
            string strZID = context.Request("ZID");
            var dt = new Dictionary<string, object>();
            dt.Add("XM", strXMSFZ.Split('-')[0]);
            dt.Add("sfzhm", strXMSFZ.Split('-')[1]);

            dt.Add("TDMS", context.Request("TDMS"));

            dt.Add("YSSC", context.Request("YSSC"));
            dt.Add("SLL", context.Request("SLL"));
            dt.Add("DE", context.Request("DE"));
            dt.Add("CRUser", context.Request("CRUser"));
            dt.Add("DB", context.Request("DB"));
            dt.Add("ZID", context.Request("ZID"));
            dt.Add("ZName", dtUser.Rows[0]["zname"].ToString());
            dt.Add("DW2", context.Request("DW2"));
            dt.Add("DID", context.Request("DID"));
            dt.Add("DW1", context.Request("DW1"));
            dt.Add("SL2", context.Request("SL2"));
            dt.Add("SL1", context.Request("SL1"));
            dt.Add("jqname", context.Request("JQMC"));

            dt.Add("SL3", "0");
            dt.Add("SL4", "0");
            string strXZID = context.Request("XZID");
            dt.Add("XZID", strXZID);
            dt.Add("CRDate", DateTime.Now.ToString());
            dt.Add("intProcessStanceid", "0");
            dt.Add("ComID", "10334");
            string strGGZW = context.Request("TYPE");
            dt.Add("ggzw", strGGZW);
            DataTable dtXZQ = new JH_Auth_UserB().GetDTByCommand("SELECT pid,pname FROM  qj_cz WHERE ID= '" + strXZID + "'");
            dt.Add("cid", dtXZQ.Rows[0]["pid"].ToString());
            dt.Add("cname", dtXZQ.Rows[0]["pname"].ToString());

            if (context.Request("CZNAME").Split('/').Length > 3)
            {
                dt.Add("CZNAME", context.Request("CZNAME").Split('/')[3]);

            }

            DataTable dtLYJG = new JH_Auth_UserB().GetDTByCommand("SELECT GYJG,jg.Type  FROM qj_sfjs_jg JG INNER  JOIN qj_sfjs_de DE ON jg.Type= DE.type and jg.zid =DE.zid WHERE jg.ZID = '" + strZID + "' AND DE.dymc = '" + strGGZW + "' ORDER BY JG.DW");
            if (dtLYJG.Rows.Count > 0)
            {
                dt.Add("TYPE", dtLYJG.Rows[0]["Type"].ToString());


                dt.Add("ZJE", Math.Round(decimal.Parse(context.Request("ZJE")), 2, MidpointRounding.AwayFromZero));
                dt.Add("GYSJ", dtLYJG.Rows[0]["GYJG"].ToString());

                decimal GYZJE = decimal.Parse(context.Request("SL1")) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString()) + decimal.Parse(context.Request("SL2")) * decimal.Parse(dtLYJG.Rows[0]["GYJG"].ToString());
                dt.Add("GYZJE", dtLYJG.Rows[0]["GYJG"].ToString());

            }

            db.InserData(dt, "qj_sfjs_ysdata");
            msg.Result = "上报成功";


        }


        /// <summary>
        /// 获取当前账号上报历史数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string XM = context.Request("XM") ?? "";

            DataTable dtUser = GetUserInfo(UP);
            string strUser = UP.Split(',')[0];

            if (dtUser.Rows.Count > 0)
            {
                if (XM == "")
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE CRUser='" + strUser + "' ORDER BY CRDate  ");

                }
                else
                {
                    msg.Result = new JH_Auth_QYB().GetDTByCommand("SELECT * FROM vwSBData WHERE CRUser='" + strUser + "' AND XM LIKE '%" + XM + "%' ORDER BY CRDate  ");

                }

            }

        }
        /// <summary>
        /// ADDYH
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP);

            string strSFZBM = context.Request("SFZBM");
            if (string.IsNullOrEmpty(strSFZBM))
            {
                msg.ErrorMsg = "身份证号码不能为空";
                return;
            }
            if (strSFZBM.Length != 18)
            {
                msg.ErrorMsg = "必须为18位身份证号码";
                return;
            }
            if (string.IsNullOrEmpty(context.Request("YHName")))
            {
                msg.ErrorMsg = "姓名不能为空";
                return;
            }


            string ID = context.Request("ID") ?? "0";
            DBFactory db = new BI_DB_SourceB().GetDB(0);
            var dt = new Dictionary<string, object>();
            dt.Add("YHName", context.Request("YHName"));
            dt.Add("sfzbm", context.Request("SFZBM"));
            dt.Add("lxdh", context.Request("LXDH"));
            dt.Add("dlpassword", strSFZBM.Substring(13));
            dt.Add("xzid", context.Request("XZID"));
            dt.Add("xzname", context.Request("XZName").Split('/')[3].ToString());
            if (ID != "0")
            {
                dt.Add("id", ID);
                db.UpdateData(dt, "qj_sfjs_jfuser");
            }
            else
            {
                dt.Add("zid", dtUser.Rows[0]["zid"].ToString());
                dt.Add("zname", dtUser.Rows[0]["zname"].ToString());
                dt.Add("CRUser", dtUser.Rows[0]["username"].ToString());
                dt.Add("DCode", dtUser.Rows[0]["zid"].ToString());
                dt.Add("DName", dtUser.Rows[0]["zname"].ToString());
                dt.Add("CRUserName", dtUser.Rows[0]["userrealname"].ToString());
                dt.Add("CRDate", DateTime.Now.ToString());
                dt.Add("intProcessStanceid", "0");
                dt.Add("lsmj", "0");
                dt.Add("jjmj", "0");
                dt.Add("zmj", "0");
                dt.Add("isyx", "Y");
                dt.Add("ComID", "10334");
                db.InserData(dt, "qj_sfjs_jfuser");
            }
        }




        public void DELYH(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string UP = context.Request("UP") ?? "";
            DataTable dtUser = GetUserInfo(UP);

            string strxzid = P1;
            new JH_Auth_UserB().ExsSclarSql("DELETE qj_sfjs_jfuser WHERE ID='" + P1 + "'");
            msg.Result = "删除用户成功";

        }


        /// <summary>
        /// 统计数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBTJLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string UP = context.Request("UP");
            string XM = context.Request("XM") ?? "";
            string strUser = UP.Split(',')[0];

            string SDATE = context.Request("SDATE") ?? "";
            string EDATE = context.Request("EDATE") ?? "";

            string strWhere = " AND  CRDate BETWEEN '" + SDATE + " 00:00:00' AND  '" + EDATE + " 23:59:00'";

            //获取价格
            DataTable DT = new JH_Auth_UserB().GetDTByCommand("  SELECT CZNAME, SUM(cast(TDMS as decimal(10, 1))) AS TDMS, SUM(cast(YSSC as decimal(10, 0))) AS YSSC,  SUM(ZSL) AS SLL, SUM(cast(ZJE as decimal(10, 2))) AS ZJE FROM vwSBData WHERE CRUser = '" + strUser + "' " + strWhere + " GROUP BY  CZNAME ");
            msg.Result = DT;

        }


        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="strUserAndPass"></param>
        /// <returns></returns>
        public DataTable GetUserInfo(string strUserAndPass)
        {
            string username = strUserAndPass.Split(',')[0];
            string password = strUserAndPass.Split(',')[1];
            DataTable userList = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM qj_xzuser where czusername='" + username + "' and password='" + password + "'");

            DataTable userListjfyh = new JH_Auth_UserB().GetDTByCommand("SELECT  * FROM qj_sfjs_jfuser WHERE sfzbm='" + username + "' AND dlpassword='" + password + "'");

            password = CommonHelp.GetMD5(password);
            DataTable userListgly = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM jh_auth_user where username='" + username + "' and UserPass='" + password + "'");



            DataTable dtyh = new DataTable();
            dtyh.Columns.Add("usertype");
            dtyh.Columns.Add("username");
            dtyh.Columns.Add("password");
            dtyh.Columns.Add("userrealname");
            dtyh.Columns.Add("userbranch");
            dtyh.Columns.Add("zid");
            dtyh.Columns.Add("zname");
            dtyh.Columns.Add("lxfs");
            dtyh.Columns.Add("czids");
            for (int i = 0; i < userList.Rows.Count; i++)
            {
                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["usertype"] = "村组管理员";
                newRow["username"] = userList.Rows[0]["czusername"].ToString();
                newRow["password"] = userList.Rows[0]["password"].ToString();
                newRow["userrealname"] = userList.Rows[0]["userrealname"].ToString();
                newRow["userbranch"] = userList.Rows[0]["zname"].ToString();
                newRow["zid"] = userList.Rows[0]["zid"].ToString();
                newRow["zname"] = userList.Rows[0]["zname"].ToString();
                newRow["lxfs"] = userList.Rows[0]["lxfs"].ToString();
                newRow["czids"] = userList.Rows[0]["czids"].ToString();

                dtyh.Rows.Add(newRow);
            }
            for (int i = 0; i < userListgly.Rows.Count; i++)
            {
                String strUserName = userListgly.Rows[0]["username"].ToString();
                string strRoleName = new JH_Auth_UserRoleB().GetRoleNameByUserName(strUserName, 10334);


                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["username"] = userListgly.Rows[0]["username"].ToString();
                newRow["password"] = userListgly.Rows[0]["userpass"].ToString();
                newRow["userrealname"] = userListgly.Rows[0]["userrealname"].ToString();
                newRow["lxfs"] = userListgly.Rows[0]["mobphone"].ToString();
                newRow["userbranch"] = userListgly.Rows[0]["branchCode"].ToString();
                if (strRoleName.Contains("斗长"))
                {
                    newRow["usertype"] = "斗长";//系统用户

                    DataTable DData = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_db  where dgly='" + strUserName + "' FOR XML PATH('')),1,1,'')  AS DBIDS;");

                    DataTable GLZata = new JH_Auth_UserB().GetDTByCommand("select TOP 1  zid,zname  from qj_db  where dgly='" + strUserName + "' ");
                    newRow["zid"] = GLZata.Rows[0]["zid"].ToString();
                    newRow["zname"] = GLZata.Rows[0]["zname"].ToString();
                    if (DData.Rows.Count > 0)
                    {
                        string strDIDS = DData.Rows[0]["DBIDS"].ToString();
                        DataTable XZata = new JH_Auth_UserB().GetDTByCommand("select STUFF((select ',' + cast(ID as varchar) from qj_cz  where did IN ('" + strDIDS.ToFormatLike() + "') FOR XML PATH('')),1,1,'')  AS XZIDS;");
                        if (XZata.Rows.Count > 0)
                        {
                            newRow["czids"] = XZata.Rows[0]["XZIDS"].ToString();
                        }
                    }
                }
                else
                {
                    newRow["usertype"] = "管理局人员";//系统用户
                    newRow["zid"] = "";
                    newRow["zname"] = "";
                    newRow["czids"] = "";

                }
                dtyh.Rows.Add(newRow);


            }


            for (int i = 0; i < userListjfyh.Rows.Count; i++)
            {
                DataRow newRow;
                newRow = dtyh.NewRow();
                newRow["usertype"] = "用水户";//系统用户
                newRow["username"] = userListjfyh.Rows[0]["sfzbm"].ToString();
                newRow["password"] = userListjfyh.Rows[0]["dlpassword"].ToString();
                newRow["userrealname"] = userListjfyh.Rows[0]["YHName"].ToString();
                newRow["lxfs"] = userListjfyh.Rows[0]["lxdh"].ToString();
                newRow["userbranch"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zid"] = userListjfyh.Rows[0]["zid"].ToString();
                newRow["zname"] = userListjfyh.Rows[0]["zname"].ToString();
                newRow["czids"] = userListjfyh.Rows[0]["xzid"].ToString();
                dtyh.Rows.Add(newRow);
            }
            return dtyh;
        }






        /// <summary>
        /// 获取上报数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSBDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            //获取上报
            string SFZHM = P1.Trim();
            string TYPE = context.Request("TYPE") ?? "0";
            string UP = context.Request("UP") ?? "";
            DataTable dt = new DataTable();
            if (TYPE == "0")
            {
                DateTime datest = DateTime.Now.AddMonths(-5);
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + UP.Split(',')[0] + "' AND CRDate>'" + datest.ToString() + "' ORDER BY CRDate DESC");

            }
            else
            {
                DateTime datest = DateTime.Now.AddMonths(-12);
                dt = new JH_Auth_UserB().GetDTByCommand("SELECT * FROM vwSBData WHERE sfzhm='" + UP.Split(',')[0] + "' AND CRDate>'" + datest.ToString() + "' ORDER BY CRDate DESC");

            }
            if (dt.Rows.Count > 0)
            {
                msg.Result = dt;
            }
            else
            {
                msg.ErrorMsg = "没找到灌溉信息";

            }


        }




        /// <summary>
        /// 复制数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void COPYDATA(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            DataTable dtglz = new JH_Auth_UserB().GetDTByCommand("SELECT * from jh_auth_branch where  DeptRoot NOT IN (-1,1) AND DeptCode in (" + P1 + ") ");
            string TYPE = context.Request("TYPE") ?? "JG";
            if (TYPE == "JG")
            {
                for (int m = 0; m < dtglz.Rows.Count; m++)
                {
                    //复制价格
                    DataTable dtJG = new JH_Auth_UserB().GetDTByCommand("SELECT * from qj_sfjs_jg where ID IN (" + P2 + ")");
                    for (int i = 0; i < dtJG.Rows.Count; i++)
                    {
                        new JH_Auth_UserB().ExsSclarSql("INSERT INTO [qj_sfjs_jg] ( [CRUser], [CRDate], [intProcessStanceid], [ComID], [Type], [DW], [jg], [gyjg], [zid], [qjjg], [DCode], [DName], [CRUserName], [zname], [jqmc]) VALUES " +
                            "( N'administrator', '" + DateTime.Now.ToString() + "', '0', '10334', N'" + dtJG.Rows[i]["Type"].ToString() + "', N'" + dtJG.Rows[i]["DW"].ToString() + "', '" + dtJG.Rows[i]["jg"].ToString() + "', '" + dtJG.Rows[i]["gyjg"].ToString() + "', '" + dtglz.Rows[m]["DeptCode"].ToString() + "', NULL, N'" + dtglz.Rows[m]["DeptCode"].ToString() + "', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', N'administrator', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', '" + dtJG.Rows[i]["jqmc"].ToString() + "');");
                    }
                }

            }
            else
            {
                for (int m = 0; m < dtglz.Rows.Count; m++)
                {
                    DataTable dtDE = new JH_Auth_UserB().GetDTByCommand("SELECT * from qj_sfjs_de where ID IN (" + P2 + ")");
                    for (int i = 0; i < dtDE.Rows.Count; i++)
                    {
                        new JH_Auth_UserB().ExsSclarSql("INSERT INTO [qj_sfjs_de] ( [CRUser], [CRDate], [intProcessStanceid], [ComID], [type], [dw], [zid], [remark], [DCode], [DName], [CRUserName], [zname], [dymc])  VALUES (" +
                                                                                 " N'administrator', '" + DateTime.Now.ToString() + "', '0', '10334', N'" + dtDE.Rows[i]["type"].ToString() + "', N'" + dtDE.Rows[i]["dw"].ToString() + "', '" + dtglz.Rows[m]["DeptCode"].ToString() + "', NULL, N'" + dtglz.Rows[m]["DeptCode"].ToString() + "', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', N'administrator', N'" + dtglz.Rows[m]["DeptName"].ToString() + "', '" + dtDE.Rows[i]["dymc"].ToString() + "');");
                    }
                }


            }


        }



        /// <summary>
        /// 复制数据
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void SETSJSM(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {

            string TYPE = context.Request("TYPE") ?? "JG";
            JH_Auth_QY QY = new JH_Auth_QYB().GetALLEntities().FirstOrDefault();
            if (P2 == "0")//获取水价说明
            {
                msg.Result = QY;

            }
            else //更新水价说明
            {
                if (P1 != "")
                {
                    QY.wxqrcode = P1;
                    new JH_Auth_QYB().Update(QY);
                    msg.Result = QY;
                }


            }



        }
        #endregion

    }
}